using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace QuizGame
{
    public class Card : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField, Range(0, 1)]
        private float _colorSaturation = 0.2f;
        [SerializeField]
        private SpriteRenderer _background;
        [SerializeField]
        private SpriteRenderer _content;
        private bool _isAnswer;

        public UnityEvent<Card> onCorrectAnswer;
        public UnityEvent<Card> onWrongAnswer;

        public void Init(CardData card, bool isAnswer)
        {
            _isAnswer = isAnswer;
            _background.color = Color.HSVToRGB(Random.value, _colorSaturation, 1);
            _content.transform.localRotation = Quaternion.Euler(0, 0, card.RotationDegrees);
            _content.sprite = card.Sprite;
            name = card.Identifier;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (_isAnswer)
            {
                onCorrectAnswer.Invoke(this);
            }
            else
            {
                onWrongAnswer.Invoke(this);
            }
        }
    }
}