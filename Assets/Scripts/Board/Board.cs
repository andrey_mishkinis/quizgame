using UnityEngine;
using UnityEngine.Events;

namespace QuizGame
{
    public class Board : MonoBehaviour
    {
        [SerializeField]
        private Card _cardPrefab;

        public UnityEvent<Card> onCorrectAnswer;
        public UnityEvent<Card> onWrongAnswer;

        public void LoadLevel(LevelData level)
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }

            var offset = new Vector3(level.Columns - 1, level.Rows - 1, 0) / 2;

            for (int row = 0, index = 0; row < level.Rows; row++)
            {
                for (var column = 0; column < level.Columns; column++, index++)
                {
                    var cardData = level.Cards[index];

                    var card = Instantiate(_cardPrefab, new Vector3(column, row) - offset, Quaternion.identity, transform);
                    card.Init(cardData, isAnswer: cardData == level.Answer);

                    card.onCorrectAnswer.AddListener(onCorrectAnswer.Invoke);
                    card.onWrongAnswer.AddListener(onWrongAnswer.Invoke);
                }
            }
        }
    }
}