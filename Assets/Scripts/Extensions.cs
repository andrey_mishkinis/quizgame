using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuizGame
{
    public static class Extensions
    {
        public static T Shuffle<T>(this T list) where T : IList
        {
            var last = list.Count;
            while (last > 1)
            {
                var next = Random.Range(0, last--);
                var temp = list[last];
                list[last] = list[next];
                list[next] = temp;
            }
            return list;
        }

        public static T GetRandom<T>(this IList<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static int GetRandomIndex(this IList list)
        {
            return Random.Range(0, list.Count);
        }
    }
}