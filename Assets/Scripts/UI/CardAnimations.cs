using DG.Tweening;
using UnityEngine;

namespace QuizGame
{
    public class CardAnimations : MonoBehaviour
    {
        [SerializeField]
        private Transform _target;

        private Tweener _tweener;

        public void PlayCorrectAnswer()
        {
            _tweener?.Kill();
            _target.localScale = Vector3.one;
            _tweener = _target.DOPunchScale(0.5f * Vector3.one, 1, 5, 0);
        }

        public void PlayWrongAnswer()
        {
            _tweener?.Kill();
            _target.localPosition = Vector3.zero;
            _tweener = _target.DOShakePosition(1, new Vector3(0.1f, 0), randomness: 0);
        }
    }
}