using UnityEngine;
using UnityEngine.UI;

namespace QuizGame
{
    public class LevelTask : MonoBehaviour
    {
        [SerializeField]
        private Text _label;

        [SerializeField]
        private string _action;

        public void SetAnswer(LevelData level)
        {
            _label.text = _action + " " + level.Answer.Identifier;
        }
    }
}