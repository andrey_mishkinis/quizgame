using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace QuizGame
{
    public class IntroAnimation : MonoBehaviour
    {
        [SerializeField]
        private Board _board;
        [SerializeField]
        private Text _taskLabel;

        public void Play()
        {
            foreach (Transform child in _board.transform)
            {
                child.DOScale(1, 1).From(0).SetEase(Ease.OutBounce);
            }
            _taskLabel.DOFade(1, 3).From(0).SetEase(Ease.InOutCubic);
        }
    }
}