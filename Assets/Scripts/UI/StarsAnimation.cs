using UnityEngine;

namespace QuizGame
{
    public class StarsAnimation : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem _particles;

        public void PlayAt(Card card)
        {
            transform.localPosition = card.transform.localPosition;
            _particles.Clear();
            _particles.Play();
        }
    }
}