using UnityEngine;
using UnityEngine.UI;

namespace QuizGame
{
    public class AutoClickTimer : MonoBehaviour
    {
        [SerializeField]
        private Button _button;

        [SerializeField]
        private float _timeout;

        private float _elapsed;
        private bool _clicked;

        public void OnEnable()
        {
            _elapsed = 0;
            _clicked = false;
        }

        public void Update()
        {
            if (!_clicked)
            {
                _elapsed += Time.deltaTime;

                if (_elapsed > _timeout)
                {
                    _button.onClick.Invoke();
                    _clicked = true;
                }
            }
        }
    }
}