using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace QuizGame
{
    public class OutroAnimation : MonoBehaviour
    {
        [SerializeField]
        private Image _fadeScreen;
        [SerializeField]
        private GameObject _splashScreen;
        private Sequence _sequence;

        public UnityEvent onComplete;

        public void Play()
        {
            _sequence?.Kill();
            _sequence = DOTween.Sequence();

            _sequence.AppendCallback(() => _fadeScreen.gameObject.SetActive(true));
            _sequence.Append(_fadeScreen.DOFade(1, 0.3f).From(0));

            _sequence.AppendCallback(() => _splashScreen.SetActive(true));
            _sequence.Append(_fadeScreen.DOFade(0, 0.3f).From(1));

            _sequence.AppendInterval(1);

            _sequence.Append(_fadeScreen.DOFade(1, 0.3f).From(0));
            _sequence.AppendCallback(() => _splashScreen.SetActive(false));

            _sequence.AppendCallback(onComplete.Invoke);

            _sequence.Append(_fadeScreen.DOFade(0, 0.3f).From(1));
            _sequence.AppendCallback(() => _fadeScreen.gameObject.SetActive(false));
        }
    }
}