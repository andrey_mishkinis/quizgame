using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace QuizGame
{
    public class LevelsController : MonoBehaviour
    {
        [SerializeField]
        private LevelConfigurationData[] _levels;
        private LevelGenerator[] _generators;
        private int _currentLevel;

        public UnityEvent<LevelData> onNextLevel;
        public UnityEvent onBeginGame;
        public UnityEvent onEndGame;

        public void Awake()
        {
            _generators = _levels.Select(x => new LevelGenerator(x)).ToArray();
        }

        public void Restart()
        {
            _currentLevel = 0;
        }

        public void LoadNextLevel()
        {
            if (_currentLevel < _generators.Length)
            {
                onNextLevel.Invoke(_generators[_currentLevel].CreateNext());

                if (_currentLevel == 0)
                {
                    onBeginGame.Invoke();
                }
                _currentLevel++;
            }
            else
            {
                onEndGame.Invoke();
            }
        }
    }
}