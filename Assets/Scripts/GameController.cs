using DG.Tweening;
using UnityEngine;

namespace QuizGame
{
    public class GameController : MonoBehaviour
    {
        [SerializeField]
        private LevelsController _levels;

        public void Start()
        {
            DOTween.Init(true, true);
            _levels.Restart();
            _levels.LoadNextLevel();
        }
    }
}