using System.Collections.Generic;
using System.Linq;

namespace QuizGame
{
    public class LevelGenerator
    {
        private readonly LevelConfigurationData _config;
        private readonly CardData[] _cards;
        private int _index;

        public LevelGenerator(LevelConfigurationData config)
        {
            _config = config;
            _cards = config.CardBundles.SelectMany(x => x.CardData).ToArray();
            _index = _cards.Length;
        }

        public LevelData CreateNext()
        {
            if (++_index >= _cards.Length)
            {
                _cards.Shuffle();
                _index = 0;
            }
            var answer = _cards[_index];

            var allowDuplicates = _config.Columns * _config.Rows > _cards.Length;
            var selectedCards = allowDuplicates ? SelectCardsWithDuplicates(answer) : SelectCardsWithoutDuplicates(answer);

            return new LevelData(_config.Columns, _config.Rows, answer, selectedCards);
        }

        private CardData[] SelectCardsWithDuplicates(CardData answer)
        {
            var selectedCardsCount = _config.Columns * _config.Rows;
            var selectedCards = new List<CardData>();
            selectedCards.Add(answer);

            var index = _cards.GetRandomIndex();
            while (selectedCards.Count < selectedCardsCount)
            {
                var card = _cards[index];
                if (card != answer)
                {
                    selectedCards.Add(card);
                }
                if (++index >= _cards.Length)
                {
                    index = 0;
                }
            }
            return selectedCards.ToArray().Shuffle();
        }

        private CardData[] SelectCardsWithoutDuplicates(CardData answer)
        {
            var selectedCardsCount = _config.Columns * _config.Rows;
            var selectedCards = new HashSet<CardData>();

            selectedCards.Add(answer);

            while (selectedCards.Count < selectedCardsCount)
            {
                selectedCards.Add(_cards.GetRandom());
            }
            return selectedCards.ToArray().Shuffle();
        }
    }
}