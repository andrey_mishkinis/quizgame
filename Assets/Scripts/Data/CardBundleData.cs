using UnityEngine;

namespace QuizGame
{
    [CreateAssetMenu]
    public class CardBundleData : ScriptableObject
    {
        [SerializeField]
        private CardData[] _cardData;

        public CardData[] CardData => _cardData;
    }
}