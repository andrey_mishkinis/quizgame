using System;
using UnityEngine;

namespace QuizGame
{
    [Serializable]
    public class CardData
    {
        [SerializeField]
        private string _identifier;

        [SerializeField]
        private Sprite _sprite;

        [SerializeField]
        private RotationType _rotation;

        public string Identifier => _identifier;

        public Sprite Sprite => _sprite;

        public RotationType Rotation => _rotation;
        public int RotationDegrees => (int)_rotation * 90;

        public enum RotationType
        {
            Rotate0,
            Rotate90,
            Rotate180,
            Rotate270,
        }
    }
}