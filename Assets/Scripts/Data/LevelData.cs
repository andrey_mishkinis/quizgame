
namespace QuizGame
{
    public class LevelData
    {
        public readonly int Columns;
        public readonly int Rows;
        public readonly CardData Answer;
        public readonly CardData[] Cards;

        public LevelData(int columns, int rows, CardData answer, CardData[] cards)
        {
            Columns = columns;
            Rows = rows;
            Answer = answer;
            Cards = cards;
        }
    }
}