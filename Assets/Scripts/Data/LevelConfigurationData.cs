using UnityEngine;

namespace QuizGame
{
    [CreateAssetMenu]
    public class LevelConfigurationData : ScriptableObject
    {
        [SerializeField]
        private int _columns;

        [SerializeField]
        private int _rows;

        [SerializeField]
        private CardBundleData[] _cardBundles;

        public int Columns => _columns;
        public int Rows => _rows;

        public CardBundleData[] CardBundles => _cardBundles;
    }
}